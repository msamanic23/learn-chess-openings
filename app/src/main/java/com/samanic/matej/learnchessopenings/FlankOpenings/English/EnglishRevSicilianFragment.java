package com.samanic.matej.learnchessopenings.FlankOpenings.English;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EnglishRevSicilianFragment extends Fragment {


    public EnglishRevSicilianFragment() {
        // Required empty public constructor
    }

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.english_rev_sicilian, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wCPawn = v.findViewById(R.id.whiteCPawn);
        final View bEPawn = v.findViewById(R.id.blackEPawn);

        final TextView tw = (TextView) v.findViewById(R.id.englishrevsic);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wCPawn.setVisibility(View.VISIBLE);
                bEPawn.setVisibility(View.VISIBLE);

                ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                ImageView e5 = (ImageView) v.findViewById(R.id.e5);

                c4.setVisibility(View.INVISIBLE);
                e5.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number){

                    case 2:
                        bEPawn.setVisibility(View.VISIBLE);
                        ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                        e5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wCPawn.setVisibility(View.VISIBLE);
                        ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                        c4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (number) {

                    case 0:
                        wCPawn.setVisibility(View.INVISIBLE);
                        ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                        c4.setImageResource(R.mipmap.white_pawn);
                        c4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bEPawn.setVisibility(View.INVISIBLE);
                        ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                        e5.setImageResource(R.mipmap.black_pawn);
                        e5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wCPawn.setVisibility(View.INVISIBLE);
                bEPawn.setVisibility(View.INVISIBLE);

                ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                ImageView e5 = (ImageView) v.findViewById(R.id.e5);

                c4.setImageResource(R.mipmap.white_pawn);
                e5.setImageResource(R.mipmap.black_pawn);

                c4.setVisibility(View.VISIBLE);
                e5.setVisibility(View.VISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 2;
            }
        });

        return v;
    }
}