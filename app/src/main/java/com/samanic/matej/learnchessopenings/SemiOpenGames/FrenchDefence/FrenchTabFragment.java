package com.samanic.matej.learnchessopenings.SemiOpenGames.FrenchDefence;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FrenchTabFragment extends Fragment {


    public TabLayout tabLayout;
    public ViewPager viewPager;
    public static int int_items = 7 ;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        View x =  inflater.inflate(R.layout.tab_layout_3, null);
        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);

        /**
         *Set an Apater for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable.
         * Maybe a Support Library Bug .
         */

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });

        return x;

    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position)
        {
            switch (position){
                case 0 : return new FrenchStartingMovesFragment();
                case 1 : return new FrenchWinawerVariationFragment();
                case 2 : return new FrenchClassicalVariationFragment();
                case 3 : return new FrenchRubinsteinVariationFragment();
                case 4 : return new FrenchTarraschVariationFragment();
                case 5 : return new FrenchAdvanceVariationFragment();
                case 6 : return new FrenchExchangeVariationFragment();

            }
            return null;
        }

        @Override
        public int getCount() {

            return int_items;

        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0 :
                    return "Starting Moves";
                case 1 :
                    return "Winawer";
                case 2:
                    return "Classical";
                case 3 :
                    return "Rubinstein";
                case 4:
                    return "Tarrasch";
                case 5:
                    return "Advance";
                case 6:
                    return "Exchange";
            }
            return null;
        }
    }

}