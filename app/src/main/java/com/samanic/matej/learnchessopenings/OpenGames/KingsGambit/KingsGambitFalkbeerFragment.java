package com.samanic.matej.learnchessopenings.OpenGames.KingsGambit;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class KingsGambitFalkbeerFragment extends Fragment {


    public KingsGambitFalkbeerFragment() {
        // Required empty public constructor
    }

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.kings_gambit_falkbeer, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wEPawn = v.findViewById(R.id.whiteEPawn);
        final View bEPawn = v.findViewById(R.id.blackEPawn);
        final View wFPawn = v.findViewById(R.id.whiteFPawn);
        final View bDPawn = v.findViewById(R.id.blackDPawn);

        final TextView tw = (TextView) v.findViewById(R.id.kingsgambitfalkbeer);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wEPawn.setVisibility(View.VISIBLE);
                bEPawn.setVisibility(View.VISIBLE);
                wFPawn.setVisibility(View.VISIBLE);
                bDPawn.setVisibility(View.VISIBLE);

                ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                ImageView f4 = (ImageView) v.findViewById(R.id.f4);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);

                e4.setVisibility(View.INVISIBLE);
                e5.setVisibility(View.INVISIBLE);
                f4.setVisibility(View.INVISIBLE);
                d5.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number){

                    case 4:
                        bDPawn.setVisibility(View.VISIBLE);
                        ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                        d5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 13, 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 3:
                        wFPawn.setVisibility(View.VISIBLE);
                        ImageView f4 = (ImageView) v.findViewById(R.id.f4);
                        f4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 10, 12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 2:
                        bEPawn.setVisibility(View.VISIBLE);
                        ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                        e5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wEPawn.setVisibility(View.VISIBLE);
                        ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                        e4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (number) {
                    case 0:
                        wEPawn.setVisibility(View.INVISIBLE);
                        ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                        e4.setImageResource(R.mipmap.white_pawn);
                        e4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bEPawn.setVisibility(View.INVISIBLE);
                        ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                        e5.setImageResource(R.mipmap.black_pawn);
                        e5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 2:
                        wFPawn.setVisibility(View.INVISIBLE);
                        ImageView f4 = (ImageView) v.findViewById(R.id.f4);
                        f4.setImageResource(R.mipmap.white_pawn);
                        f4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 3:
                        bDPawn.setVisibility(View.INVISIBLE);
                        ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                        d5.setImageResource(R.mipmap.black_pawn);
                        d5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 13, 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wEPawn.setVisibility(View.INVISIBLE);
                bEPawn.setVisibility(View.INVISIBLE);
                wFPawn.setVisibility(View.INVISIBLE);
                bDPawn.setVisibility(View.INVISIBLE);

                ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                ImageView f4 = (ImageView) v.findViewById(R.id.f4);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);

                e4.setImageResource(R.mipmap.white_pawn);
                e5.setImageResource(R.mipmap.black_pawn);
                f4.setImageResource(R.mipmap.white_pawn);
                d5.setImageResource(R.mipmap.black_pawn);

                e4.setVisibility(View.VISIBLE);
                e5.setVisibility(View.VISIBLE);
                f4.setVisibility(View.VISIBLE);
                d5.setVisibility(View.VISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 4;
            }
        });

        return v;
    }
}
