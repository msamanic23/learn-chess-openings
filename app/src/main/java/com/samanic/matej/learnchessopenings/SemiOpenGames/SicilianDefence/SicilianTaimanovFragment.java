package com.samanic.matej.learnchessopenings.SemiOpenGames.SicilianDefence;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SicilianTaimanovFragment extends Fragment {


    public SicilianTaimanovFragment() {
        // Required empty public constructor
    }

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.sicilian_taimanov, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wEPawn = v.findViewById(R.id.whiteEPawn);
        final View bCPawn = v.findViewById(R.id.blackCPawn);
        final View wGKnight = v.findViewById(R.id.whiteGKnight);
        final View bEPawn = v.findViewById(R.id.blackEPawn);
        final View wDPawn = v.findViewById(R.id.whiteDPawn);
        final View bBKnight = v.findViewById(R.id.blackBKnight);

        final TextView tw = (TextView) v.findViewById(R.id.siciliantaimanov);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wEPawn.setVisibility(View.VISIBLE);
                bCPawn.setVisibility(View.VISIBLE);
                wGKnight.setVisibility(View.VISIBLE);
                wDPawn.setVisibility(View.VISIBLE);
                bBKnight.setVisibility(View.VISIBLE);
                bEPawn.setVisibility(View.VISIBLE);

                ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                ImageView c5 = (ImageView) v.findViewById(R.id.c5);
                ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                ImageView c6 = (ImageView) v.findViewById(R.id.c6);
                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView e6 = (ImageView) v.findViewById(R.id.e6);

                e4.setVisibility(View.INVISIBLE);
                c5.setVisibility(View.INVISIBLE);
                f3.setVisibility(View.INVISIBLE);
                d4.setVisibility(View.INVISIBLE);
                e6.setVisibility(View.INVISIBLE);
                c6.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number){

                    case 8:
                        bBKnight.setVisibility(View.VISIBLE);
                        ImageView c6 = (ImageView) v.findViewById(R.id.c6);
                        c6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 34, 37, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 7:
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setImageResource(R.mipmap.white_knight);
                        f3.setVisibility(View.VISIBLE);

                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setImageResource(R.mipmap.black_pawn);
                        d4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 29, 33, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 6:
                        ImageView c5 = (ImageView) v.findViewById(R.id.c5);
                        c5.setImageResource(R.mipmap.black_pawn);
                        c5.setVisibility(View.VISIBLE);

                        ImageView dd4 = (ImageView) v.findViewById(R.id.d4);
                        dd4.setImageResource(R.mipmap.white_pawn);
                        dd4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 22, 26, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 5:
                        wDPawn.setVisibility(View.VISIBLE);
                        ImageView ddd4 = (ImageView) v.findViewById(R.id.d4);
                        ddd4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 19, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 4:
                        bEPawn.setVisibility(View.VISIBLE);
                        ImageView e6 = (ImageView) v.findViewById(R.id.e6);
                        e6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 14, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 3:
                        wGKnight.setVisibility(View.VISIBLE);
                        ImageView ff3 = (ImageView) v.findViewById(R.id.f3);
                        ff3.setImageResource(R.mipmap.black_knight);
                        ff3.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 10, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 2:
                        bCPawn.setVisibility(View.VISIBLE);
                        ImageView cc5 = (ImageView) v.findViewById(R.id.c5);
                        cc5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wEPawn.setVisibility(View.VISIBLE);
                        ImageView eee4 = (ImageView) v.findViewById(R.id.e4);
                        eee4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (number) {
                    case 0:
                        wEPawn.setVisibility(View.INVISIBLE);
                        ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                        e4.setImageResource(R.mipmap.white_pawn);
                        e4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bCPawn.setVisibility(View.INVISIBLE);
                        ImageView c5 = (ImageView) v.findViewById(R.id.c5);
                        c5.setImageResource(R.mipmap.black_pawn);
                        c5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 2:
                        wGKnight.setVisibility(View.INVISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setImageResource(R.mipmap.white_knight);
                        f3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 3:
                        bEPawn.setVisibility(View.INVISIBLE);
                        ImageView e6 = (ImageView) v.findViewById(R.id.e6);
                        e6.setImageResource(R.mipmap.black_pawn);
                        e6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 14, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 4:
                        wDPawn.setVisibility(View.INVISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setImageResource(R.mipmap.white_pawn);
                        d4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 19, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 5:
                        ImageView cc5 = (ImageView) v.findViewById(R.id.c5);
                        cc5.setVisibility(View.INVISIBLE);
                        ImageView dd4 = (ImageView) v.findViewById(R.id.d4);
                        dd4.setImageResource(R.mipmap.black_pawn);
                        dd4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 22, 26, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 6:
                        ImageView ff3 = (ImageView) v.findViewById(R.id.f3);
                        ff3.setVisibility(View.INVISIBLE);
                        ImageView ddd4 = (ImageView) v.findViewById(R.id.d4);
                        ddd4.setImageResource(R.mipmap.white_knight);
                        ddd4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 29, 33, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 7:
                        bBKnight.setVisibility(View.INVISIBLE);
                        ImageView c6 = (ImageView) v.findViewById(R.id.c6);
                        c6.setImageResource(R.mipmap.black_knight);
                        c6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 34, 37, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wEPawn.setVisibility(View.INVISIBLE);
                bCPawn.setVisibility(View.INVISIBLE);
                wGKnight.setVisibility(View.INVISIBLE);
                bBKnight.setVisibility(View.INVISIBLE);
                wDPawn.setVisibility(View.INVISIBLE);
                bEPawn.setVisibility(View.INVISIBLE);

                ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                ImageView c5 = (ImageView) v.findViewById(R.id.c5);
                ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView c6 = (ImageView) v.findViewById(R.id.c6);
                ImageView e6 = (ImageView) v.findViewById(R.id.e6);

                e4.setImageResource(R.mipmap.white_pawn);
                d4.setImageResource(R.mipmap.white_knight);
                c6.setImageResource(R.mipmap.black_knight);
                e6.setImageResource(R.mipmap.black_pawn);

                e4.setVisibility(View.VISIBLE);
                c5.setVisibility(View.INVISIBLE);
                f3.setVisibility(View.INVISIBLE);
                c6.setVisibility(View.VISIBLE);
                d4.setVisibility(View.VISIBLE);
                e6.setVisibility(View.VISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 19, 26, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 29, 37, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 8;
            }
        });

        return v;
    }
}