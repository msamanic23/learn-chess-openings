package com.samanic.matej.learnchessopenings.SemiOpenGames.AlekhineDefence;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AlekhineFourPawnsAttackFragment extends Fragment {

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.alekhine_four_pawns_attack, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wEPawn = v.findViewById(R.id.whiteEPawn);
        final View bGKnight = v.findViewById(R.id.blackGKnight);
        final View wDPawn = v.findViewById(R.id.whiteDPawn);
        final View bDPawn = v.findViewById(R.id.blackDPawn);
        final View wCPawn = v.findViewById(R.id.whiteCPawn);
        final View wFPawn = v.findViewById(R.id.whiteFPawn);

        final TextView tw = (TextView) v.findViewById(R.id.alekhinefourpawns);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wEPawn.setVisibility(View.VISIBLE);
                bGKnight.setVisibility(View.VISIBLE);
                wDPawn.setVisibility(View.VISIBLE);
                bDPawn.setVisibility(View.VISIBLE);
                wCPawn.setVisibility(View.VISIBLE);
                wFPawn.setVisibility(View.VISIBLE);

                ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView d6 = (ImageView) v.findViewById(R.id.d6);
                ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                ImageView b6 = (ImageView) v.findViewById(R.id.b6);
                ImageView f4 = (ImageView) v.findViewById(R.id.f4);

                e4.setVisibility(View.INVISIBLE);
                f6.setVisibility(View.INVISIBLE);
                e5.setVisibility(View.INVISIBLE);
                d5.setVisibility(View.INVISIBLE);
                d4.setVisibility(View.INVISIBLE);
                d6.setVisibility(View.INVISIBLE);
                c4.setVisibility(View.INVISIBLE);
                b6.setVisibility(View.INVISIBLE);
                f4.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number){

                    case 9:
                        ImageView f4 = (ImageView) v.findViewById(R.id.f4);
                        f4.setVisibility(View.INVISIBLE);
                        wFPawn.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 37, 39, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 8:
                        ImageView b6 = (ImageView) v.findViewById(R.id.b6);
                        b6.setVisibility(View.INVISIBLE);
                        ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                        d5.setImageResource(R.mipmap.black_knight);
                        d5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 31, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 7:
                        wCPawn.setVisibility(View.VISIBLE);
                        ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                        c4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 28, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 6:
                        bDPawn.setVisibility(View.VISIBLE);
                        ImageView dd6 = (ImageView) v.findViewById(R.id.d6);
                        dd6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 23, 25, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 5:
                        wDPawn.setVisibility(View.VISIBLE);
                        ImageView dd4 = (ImageView) v.findViewById(R.id.d4);
                        dd4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 20, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 4:
                        ImageView dd5 = (ImageView) v.findViewById(R.id.d5);
                        dd5.setVisibility(View.INVISIBLE);
                        ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                        f6.setImageResource(R.mipmap.black_knight);
                        f6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 14, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 3:
                        ImageView ee4 = (ImageView) v.findViewById(R.id.e4);
                        ee4.setImageResource(R.mipmap.white_pawn);
                        ee4.setVisibility(View.VISIBLE);
                        ImageView ee5 = (ImageView) v.findViewById(R.id.e5);
                        ee5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 11, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 2:
                        bGKnight.setVisibility(View.VISIBLE);
                        ImageView ff6 = (ImageView) v.findViewById(R.id.f6);
                        ff6.setImageResource(R.mipmap.black_knight);
                        ff6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 5, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wEPawn.setVisibility(View.VISIBLE);
                        ImageView eee4 = (ImageView) v.findViewById(R.id.e4);
                        eee4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (number){
                    case 0:
                        wEPawn.setVisibility(View.INVISIBLE);
                        ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                        e4.setImageResource(R.mipmap.white_pawn);
                        e4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bGKnight.setVisibility(View.INVISIBLE);
                        ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                        f6.setImageResource(R.mipmap.black_knight);
                        f6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 5, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 2:
                        ImageView ee4 = (ImageView) v.findViewById(R.id.e4);
                        ee4.setVisibility(View.INVISIBLE);
                        ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                        e5.setImageResource(R.mipmap.white_pawn);
                        e5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 11, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 3:
                        ImageView fe6 = (ImageView) v.findViewById(R.id.f6);
                        fe6.setVisibility(View.INVISIBLE);
                        ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                        d5.setImageResource(R.mipmap.black_knight);
                        d5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 14, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 4:
                        wDPawn.setVisibility(View.INVISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setImageResource(R.mipmap.white_pawn);
                        d4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 20, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 5:
                        bDPawn.setVisibility(View.INVISIBLE);
                        ImageView d6 = (ImageView) v.findViewById(R.id.d6);
                        d6.setImageResource(R.mipmap.black_pawn);
                        d6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 23, 25, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 6:
                        wCPawn.setVisibility(View.INVISIBLE);
                        ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                        c4.setImageResource(R.mipmap.white_pawn);
                        c4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 28, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 7:
                        ImageView dd5 = (ImageView) v.findViewById(R.id.d5);
                        dd5.setVisibility(View.INVISIBLE);
                        ImageView b6 = (ImageView) v.findViewById(R.id.b6);
                        b6.setImageResource(R.mipmap.black_knight);
                        b6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 31, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 8:
                        wFPawn.setVisibility(View.INVISIBLE);
                        ImageView f4 = (ImageView) v.findViewById(R.id.f4);
                        f4.setImageResource(R.mipmap.white_pawn);
                        f4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 37, 39, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wEPawn.setVisibility(View.INVISIBLE);
                bGKnight.setVisibility(View.INVISIBLE);
                wDPawn.setVisibility(View.INVISIBLE);
                bDPawn.setVisibility(View.INVISIBLE);
                wCPawn.setVisibility(View.INVISIBLE);
                wFPawn.setVisibility(View.INVISIBLE);

                ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView d6 = (ImageView) v.findViewById(R.id.d6);
                ImageView b6 = (ImageView) v.findViewById(R.id.b6);
                ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                ImageView f4 = (ImageView) v.findViewById(R.id.f4);
                ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);

                c4.setImageResource(R.mipmap.white_pawn);
                e5.setImageResource(R.mipmap.white_pawn);
                d6.setImageResource(R.mipmap.black_pawn);
                d4.setImageResource(R.mipmap.white_pawn);
                b6.setImageResource(R.mipmap.black_knight);
                f4.setImageResource(R.mipmap.white_pawn);

                c4.setVisibility(View.VISIBLE);
                d4.setVisibility(View.VISIBLE);
                d6.setVisibility(View.VISIBLE);
                b6.setVisibility(View.VISIBLE);
                f4.setVisibility(View.VISIBLE);
                e5.setVisibility(View.VISIBLE);
                f6.setVisibility(View.INVISIBLE);
                e4.setVisibility(View.INVISIBLE);
                d5.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 11, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 20, 25, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 28, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 37, 39, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 9;
            }
        });

        return v;
    }
}