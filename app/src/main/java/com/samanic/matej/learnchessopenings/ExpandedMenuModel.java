package com.samanic.matej.learnchessopenings;

public class ExpandedMenuModel {
    String iconName = "";
    int iconImg = -1; // menu icon resource id

    public String getIconName() {
        return iconName;
    }
    public void setIconName(String iconName) {
        this.iconName = iconName;
    }
}
