package com.samanic.matej.learnchessopenings.IndianDefence.GrunfeldDefence;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class GrunfeldExchangeFragment extends Fragment {


    public GrunfeldExchangeFragment() {
        // Required empty public constructor
    }

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.grunfeld_exchange, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wDPawn = v.findViewById(R.id.whiteDPawn);
        final View bGKnight = v.findViewById(R.id.blackGKnight);
        final View wCPawn = v.findViewById(R.id.whiteCPawn);
        final View bGPawn = v.findViewById(R.id.blackGPawn);
        final View wBKnight = v.findViewById(R.id.whiteBKnight);
        final View bDPawn = v.findViewById(R.id.blackDPawn);
        final View wEPawn = v.findViewById(R.id.whiteEPawn);


        final TextView tw = (TextView) v.findViewById(R.id.grunfeldexchange);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wDPawn.setVisibility(View.VISIBLE);
                bGKnight.setVisibility(View.VISIBLE);
                wCPawn.setVisibility(View.VISIBLE);
                bGPawn.setVisibility(View.VISIBLE);
                wBKnight.setVisibility(View.VISIBLE);
                bDPawn.setVisibility(View.VISIBLE);
                wEPawn.setVisibility(View.VISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                ImageView e4 = (ImageView) v.findViewById(R.id.e4);

                d4.setVisibility(View.INVISIBLE);
                f6.setVisibility(View.INVISIBLE);
                c4.setVisibility(View.INVISIBLE);
                g6.setVisibility(View.INVISIBLE);
                c3.setVisibility(View.INVISIBLE);
                d5.setVisibility(View.INVISIBLE);
                e4.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number){

                    case 9:
                        wEPawn.setVisibility(View.VISIBLE);
                        ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                        e4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 40, 42, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 8:
                        ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                        f6.setImageResource(R.mipmap.black_knight);
                        f6.setVisibility(View.VISIBLE);

                        ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                        d5.setImageResource(R.mipmap.white_pawn);
                        d5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 33, 37, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 7:
                        ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                        c4.setImageResource(R.mipmap.white_pawn);
                        c4.setVisibility(View.VISIBLE);

                        ImageView dd5 = (ImageView) v.findViewById(R.id.d5);
                        dd5.setImageResource(R.mipmap.black_pawn);
                        dd5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 28, 32, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 6:
                        bDPawn.setVisibility(View.VISIBLE);
                        ImageView ddd5 = (ImageView) v.findViewById(R.id.d5);
                        ddd5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 23, 25, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 5:
                        wBKnight.setVisibility(View.VISIBLE);
                        ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                        c3.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 19, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 4:
                        bGPawn.setVisibility(View.VISIBLE);
                        ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                        g6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 14, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 3:
                        wCPawn.setVisibility(View.VISIBLE);
                        ImageView cc4 = (ImageView) v.findViewById(R.id.c4);
                        cc4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 11, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 2:
                        bGKnight.setVisibility(View.VISIBLE);
                        ImageView ff6 = (ImageView) v.findViewById(R.id.f6);
                        ff6.setImageResource(R.mipmap.black_knight);
                        ff6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 5, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wDPawn.setVisibility(View.VISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (number) {
                    case 0:
                        wDPawn.setVisibility(View.INVISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setImageResource(R.mipmap.white_pawn);
                        d4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bGKnight.setVisibility(View.INVISIBLE);
                        ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                        f6.setImageResource(R.mipmap.black_knight);
                        f6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 5, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 2:
                        wCPawn.setVisibility(View.INVISIBLE);
                        ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                        c4.setImageResource(R.mipmap.white_pawn);
                        c4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 11, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 3:
                        bGPawn.setVisibility(View.INVISIBLE);
                        ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                        g6.setImageResource(R.mipmap.black_pawn);
                        g6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 14, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 4:
                        wBKnight.setVisibility(View.INVISIBLE);
                        ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                        c3.setImageResource(R.mipmap.white_knight);
                        c3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 19, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 5:
                        bDPawn.setVisibility(View.INVISIBLE);
                        ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                        d5.setImageResource(R.mipmap.black_pawn);
                        d5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 23, 25, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 6:
                        ImageView cc4 = (ImageView) v.findViewById(R.id.c4);
                        cc4.setVisibility(View.INVISIBLE);

                        ImageView dd5 = (ImageView) v.findViewById(R.id.d5);
                        dd5.setImageResource(R.mipmap.white_pawn);
                        dd5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 28, 32, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 7:
                        ImageView ff6 = (ImageView) v.findViewById(R.id.f6);
                        ff6.setVisibility(View.INVISIBLE);

                        ImageView ddd5 = (ImageView) v.findViewById(R.id.d5);
                        ddd5.setImageResource(R.mipmap.black_knight);
                        ddd5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 33, 37, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 8:
                        wEPawn.setVisibility(View.INVISIBLE);
                        ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                        e4.setImageResource(R.mipmap.white_pawn);
                        e4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 40, 42, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wDPawn.setVisibility(View.INVISIBLE);
                bGKnight.setVisibility(View.INVISIBLE);
                wCPawn.setVisibility(View.INVISIBLE);
                bGPawn.setVisibility(View.INVISIBLE);
                wBKnight.setVisibility(View.INVISIBLE);
                bDPawn.setVisibility(View.INVISIBLE);
                wEPawn.setVisibility(View.INVISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                ImageView e4 = (ImageView) v.findViewById(R.id.e4);

                d4.setImageResource(R.mipmap.white_pawn);
                g6.setImageResource(R.mipmap.black_pawn);
                c3.setImageResource(R.mipmap.white_knight);
                d5.setImageResource(R.mipmap.black_knight);
                e4.setImageResource(R.mipmap.white_pawn);

                d4.setVisibility(View.VISIBLE);
                f6.setVisibility(View.INVISIBLE);
                c4.setVisibility(View.INVISIBLE);
                g6.setVisibility(View.VISIBLE);
                c3.setVisibility(View.VISIBLE);
                d5.setVisibility(View.VISIBLE);
                e4.setVisibility(View.VISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 11, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 19, 25, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 28, 37, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 40, 42, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 9;
            }
        });

        return v;
    }
}