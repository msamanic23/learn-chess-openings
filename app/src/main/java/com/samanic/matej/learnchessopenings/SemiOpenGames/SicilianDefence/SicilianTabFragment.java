package com.samanic.matej.learnchessopenings.SemiOpenGames.SicilianDefence;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SicilianTabFragment extends Fragment {


    public TabLayout tabLayout;
    public ViewPager viewPager;
    public static int int_items = 8 ;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        View x =  inflater.inflate(R.layout.tab_layout_3, null);
        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);

        /**
         *Set an Apater for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });

        return x;

    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position)
        {
            switch (position){
                case 0 : return new SicilianStartingMovesFragment();
                case 1 : return new SicilianNajdorfFragment();
                case 2 : return new SicilianDragonFragment();
                case 3 : return new SicilianClassicalFragment();
                case 4 : return new SicilianScheveningenFragment();
                case 5 : return new SicilianSveshnikovFragment();
                case 6 : return new SicilianTaimanovFragment();
                case 7 : return new SicilianClosedFragment();

            }
            return null;
        }

        @Override
        public int getCount() {

            return int_items;

        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0 :
                    return "Starting Moves";
                case 1 :
                    return "Najdorf";
                case 2 :
                    return "Dragon";
                case 3 :
                    return "Classical";
                case 4 :
                    return "Scheveningen";
                case 5 :
                    return "Sveshnikov";
                case 6 :
                    return "Taimanov";
                case 7 :
                    return "Closed";
            }
            return null;
        }
    }

}