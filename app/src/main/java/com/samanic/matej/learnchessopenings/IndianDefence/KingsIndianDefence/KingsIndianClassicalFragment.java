package com.samanic.matej.learnchessopenings.IndianDefence.KingsIndianDefence;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class KingsIndianClassicalFragment extends Fragment {


    public KingsIndianClassicalFragment() {
        // Required empty public constructor
    }

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.kings_indian_classical, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        final ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wDPawn = v.findViewById(R.id.whiteDPawn);
        final View bGKnight = v.findViewById(R.id.blackGKnight);
        final View wCPawn = v.findViewById(R.id.whiteCPawn);
        final View bGPawn = v.findViewById(R.id.blackGPawn);
        final View wBKnight = v.findViewById(R.id.whiteBKnight);
        final View bDBishop = v.findViewById(R.id.blackDarkBishop);
        final View wEPawn = v.findViewById(R.id.whiteEPawn);
        final View bDPawn = v.findViewById(R.id.blackDPawn);
        final View wGKnight = v.findViewById(R.id.whiteGKnight);
        final View bKing = v.findViewById(R.id.blackKing);
        final View bHRook = v.findViewById(R.id.blackHRook);
        final View wFBishop = v.findViewById(R.id.whiteLightBishop);
        final View bEPawn = v.findViewById(R.id.blackEPawn);

        final TextView tw = (TextView) v.findViewById(R.id.kingsindianclassical);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;


        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wDPawn.setVisibility(View.VISIBLE);
                bGKnight.setVisibility(View.VISIBLE);
                wCPawn.setVisibility(View.VISIBLE);
                bGPawn.setVisibility(View.VISIBLE);
                wBKnight.setVisibility(View.VISIBLE);
                bDBishop.setVisibility(View.VISIBLE);
                wEPawn.setVisibility(View.VISIBLE);
                bDPawn.setVisibility(View.VISIBLE);
                wGKnight.setVisibility(View.VISIBLE);
                bKing.setVisibility(View.VISIBLE);
                bHRook.setVisibility(View.VISIBLE);
                wFBishop.setVisibility(View.VISIBLE);
                bEPawn.setVisibility(View.VISIBLE);


                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                ImageView g7 = (ImageView) v.findViewById(R.id.g7);
                ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                ImageView d6 = (ImageView) v.findViewById(R.id.d6);
                ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                ImageView g8 = (ImageView) v.findViewById(R.id.g8);
                ImageView f8 = (ImageView) v.findViewById(R.id.f8);
                ImageView e2 = (ImageView) v.findViewById(R.id.e2);
                ImageView e5 = (ImageView) v.findViewById(R.id.e5);

                d4.setVisibility(View.INVISIBLE);
                f6.setVisibility(View.INVISIBLE);
                c4.setVisibility(View.INVISIBLE);
                g6.setVisibility(View.INVISIBLE);
                c3.setVisibility(View.INVISIBLE);
                g7.setVisibility(View.INVISIBLE);
                e4.setVisibility(View.INVISIBLE);
                d6.setVisibility(View.INVISIBLE);
                f3.setVisibility(View.INVISIBLE);
                g8.setVisibility(View.INVISIBLE);
                f8.setVisibility(View.INVISIBLE);
                e2.setVisibility(View.INVISIBLE);
                e5.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number){

                    case 12:
                        bEPawn.setVisibility(View.VISIBLE);
                        ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                        e5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 51, 53, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 11:
                        wFBishop.setVisibility(View.VISIBLE);
                        ImageView e2 = (ImageView) v.findViewById(R.id.e2);
                        e2.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 47, 50, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 10:
                        bKing.setVisibility(View.VISIBLE);
                        bHRook.setVisibility(View.VISIBLE);
                        ImageView g8 = (ImageView) v.findViewById(R.id.g8);
                        g8.setVisibility(View.INVISIBLE);
                        ImageView f8 = (ImageView) v.findViewById(R.id.f8);
                        f8.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 41, 44, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 9:
                        wGKnight.setVisibility(View.VISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 37, 40, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 8:
                        bDPawn.setVisibility(View.VISIBLE);
                        ImageView d6 = (ImageView) v.findViewById(R.id.d6);
                        d6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 32, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 7:
                        wEPawn.setVisibility(View.VISIBLE);
                        ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                        e4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 29, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 6:
                        bDBishop.setVisibility(View.VISIBLE);
                        ImageView g7 = (ImageView) v.findViewById(R.id.g7);
                        g7.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 23, 26, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 5:
                        wBKnight.setVisibility(View.VISIBLE);
                        ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                        c3.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 19, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 4:
                        bGPawn.setVisibility(View.VISIBLE);
                        ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                        g6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 14, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 3:
                        wCPawn.setVisibility(View.VISIBLE);
                        ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                        c4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 11, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 2:
                        bGKnight.setVisibility(View.VISIBLE);
                        ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                        f6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 5, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wDPawn.setVisibility(View.VISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                switch (number) {
                    case 0:
                        wDPawn.setVisibility(View.INVISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setImageResource(R.mipmap.white_pawn);
                        d4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bGKnight.setVisibility(View.INVISIBLE);
                        ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                        f6.setImageResource(R.mipmap.black_knight);
                        f6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 5, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 2:
                        wCPawn.setVisibility(View.INVISIBLE);
                        ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                        c4.setImageResource(R.mipmap.white_pawn);
                        c4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 11, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 3:
                        bGPawn.setVisibility(View.INVISIBLE);
                        ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                        g6.setImageResource(R.mipmap.black_pawn);
                        g6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 14, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 4:
                        wBKnight.setVisibility(View.INVISIBLE);
                        ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                        c3.setImageResource(R.mipmap.white_knight);
                        c3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 19, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 5:
                        bDBishop.setVisibility(View.INVISIBLE);
                        ImageView g7 = (ImageView) v.findViewById(R.id.g7);
                        g7.setImageResource(R.mipmap.black_bishop);
                        g7.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 23, 26, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 6:
                        wEPawn.setVisibility(View.INVISIBLE);
                        ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                        e4.setImageResource(R.mipmap.white_pawn);
                        e4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 29, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 7:
                        bDPawn.setVisibility(View.INVISIBLE);
                        ImageView d6 = (ImageView) v.findViewById(R.id.d6);
                        d6.setImageResource(R.mipmap.black_pawn);
                        d6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 32, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 8:
                        wGKnight.setVisibility(View.INVISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setImageResource(R.mipmap.white_knight);
                        f3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 37, 40, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 9:
                        bKing.setVisibility(View.INVISIBLE);
                        bHRook.setVisibility(View.INVISIBLE);
                        ImageView g8 = (ImageView) v.findViewById(R.id.g8);
                        g8.setImageResource(R.mipmap.black_king);
                        g8.setVisibility(View.VISIBLE);
                        ImageView f8 = (ImageView) v.findViewById(R.id.f8);
                        f8.setImageResource(R.mipmap.black_rook);
                        f8.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 41, 44, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 10:
                        wFBishop.setVisibility(View.INVISIBLE);
                        ImageView e2 = (ImageView) v.findViewById(R.id.e2);
                        e2.setImageResource(R.mipmap.white_bishop);
                        e2.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 47, 50, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 11:
                        bEPawn.setVisibility(View.INVISIBLE);
                        ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                        e5.setImageResource(R.mipmap.black_pawn);
                        e5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 51, 53, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wDPawn.setVisibility(View.INVISIBLE);
                bGKnight.setVisibility(View.INVISIBLE);
                wCPawn.setVisibility(View.INVISIBLE);
                bGPawn.setVisibility(View.INVISIBLE);
                wBKnight.setVisibility(View.INVISIBLE);
                bDBishop.setVisibility(View.INVISIBLE);
                wEPawn.setVisibility(View.INVISIBLE);
                bDPawn.setVisibility(View.INVISIBLE);
                wGKnight.setVisibility(View.INVISIBLE);
                bKing.setVisibility(View.INVISIBLE);
                bHRook.setVisibility(View.INVISIBLE);
                wFBishop.setVisibility(View.INVISIBLE);
                bEPawn.setVisibility(View.INVISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                ImageView g7 = (ImageView) v.findViewById(R.id.g7);
                ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                ImageView d6 = (ImageView) v.findViewById(R.id.d6);
                ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                ImageView g8 = (ImageView) v.findViewById(R.id.g8);
                ImageView f8 = (ImageView) v.findViewById(R.id.f8);
                ImageView e2 = (ImageView) v.findViewById(R.id.e2);
                ImageView e5 = (ImageView) v.findViewById(R.id.e5);

                d4.setImageResource(R.mipmap.white_pawn);
                f6.setImageResource(R.mipmap.black_knight);
                c4.setImageResource(R.mipmap.white_pawn);
                g6.setImageResource(R.mipmap.black_pawn);
                c3.setImageResource(R.mipmap.white_knight);
                g7.setImageResource(R.mipmap.black_bishop);
                e4.setImageResource(R.mipmap.white_pawn);
                d6.setImageResource(R.mipmap.black_pawn);
                f3.setImageResource(R.mipmap.white_knight);
                g8.setImageResource(R.mipmap.black_king);
                f8.setImageResource(R.mipmap.black_rook);
                e2.setImageResource(R.mipmap.white_bishop);
                e5.setImageResource(R.mipmap.black_pawn);

                d4.setVisibility(View.VISIBLE);
                f6.setVisibility(View.VISIBLE);
                c4.setVisibility(View.VISIBLE);
                g6.setVisibility(View.VISIBLE);
                c3.setVisibility(View.VISIBLE);
                g7.setVisibility(View.VISIBLE);
                e4.setVisibility(View.VISIBLE);
                d6.setVisibility(View.VISIBLE);
                f3.setVisibility(View.VISIBLE);
                g8.setVisibility(View.VISIBLE);
                f8.setVisibility(View.VISIBLE);
                e2.setVisibility(View.VISIBLE);
                e5.setVisibility(View.VISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 11, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 19, 26, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 29, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 37, 44, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 47, 53, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 12;
            }
        });

        return v;
    }
}