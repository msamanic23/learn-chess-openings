package com.samanic.matej.learnchessopenings.OpenGames.RuyLopez;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RuyClassicalDefenceFragment extends Fragment {


    public RuyClassicalDefenceFragment() {
        // Required empty public constructor
    }

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.ruy_classical_defence, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wEPawn = v.findViewById(R.id.whiteEPawn);
        final View bEPawn = v.findViewById(R.id.blackEPawn);
        final View wGKnight = v.findViewById(R.id.whiteGKnight);
        final View bBKnight = v.findViewById(R.id.blackBKnight);
        final View wFBishop = v.findViewById(R.id.whiteLightBishop);
        final View bFBishop = v.findViewById(R.id.blackDarkBishop);

        final TextView tw = (TextView) v.findViewById(R.id.ruyclassical);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wEPawn.setVisibility(View.VISIBLE);
                bEPawn.setVisibility(View.VISIBLE);
                wGKnight.setVisibility(View.VISIBLE);
                bBKnight.setVisibility(View.VISIBLE);
                wFBishop.setVisibility(View.VISIBLE);
                bFBishop.setVisibility(View.VISIBLE);

                ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                ImageView c6 = (ImageView) v.findViewById(R.id.c6);
                ImageView b5 = (ImageView) v.findViewById(R.id.b5);
                ImageView c5 = (ImageView) v.findViewById(R.id.c5);

                e4.setVisibility(View.INVISIBLE);
                e5.setVisibility(View.INVISIBLE);
                f3.setVisibility(View.INVISIBLE);
                c6.setVisibility(View.INVISIBLE);
                b5.setVisibility(View.INVISIBLE);
                c5.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number){

                    case 6:
                        bFBishop.setVisibility(View.VISIBLE);
                        ImageView c5 = (ImageView) v.findViewById(R.id.c5);
                        c5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 24, 27, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 5:
                        wFBishop.setVisibility(View.VISIBLE);
                        ImageView b5 = (ImageView) v.findViewById(R.id.b5);
                        b5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 20, 23, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 4:
                        bBKnight.setVisibility(View.VISIBLE);
                        ImageView c6 = (ImageView) v.findViewById(R.id.c6);
                        c6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 14, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 3:
                        wGKnight.setVisibility(View.VISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 10, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 2:
                        bEPawn.setVisibility(View.VISIBLE);
                        ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                        e5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wEPawn.setVisibility(View.VISIBLE);
                        ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                        e4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (number) {
                    case 0:
                        wEPawn.setVisibility(View.INVISIBLE);
                        ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                        e4.setImageResource(R.mipmap.white_pawn);
                        e4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bEPawn.setVisibility(View.INVISIBLE);
                        ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                        e5.setImageResource(R.mipmap.black_pawn);
                        e5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 2:
                        wGKnight.setVisibility(View.INVISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setImageResource(R.mipmap.white_knight);
                        f3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 3:
                        bBKnight.setVisibility(View.INVISIBLE);
                        ImageView c6 = (ImageView) v.findViewById(R.id.c6);
                        c6.setImageResource(R.mipmap.black_knight);
                        c6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 14, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 4:
                        wFBishop.setVisibility(View.INVISIBLE);
                        ImageView b5 = (ImageView) v.findViewById(R.id.b5);
                        b5.setImageResource(R.mipmap.white_bishop);
                        b5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 20, 23, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 5:
                        bFBishop.setVisibility(View.INVISIBLE);
                        ImageView c5 = (ImageView) v.findViewById(R.id.c5);
                        c5.setImageResource(R.mipmap.black_bishop);
                        c5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 24, 27, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wEPawn.setVisibility(View.INVISIBLE);
                bEPawn.setVisibility(View.INVISIBLE);
                wGKnight.setVisibility(View.INVISIBLE);
                bBKnight.setVisibility(View.INVISIBLE);
                wFBishop.setVisibility(View.INVISIBLE);
                bFBishop.setVisibility(View.INVISIBLE);

                ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                ImageView e5 = (ImageView) v.findViewById(R.id.e5);
                ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                ImageView c6 = (ImageView) v.findViewById(R.id.c6);
                ImageView b5 = (ImageView) v.findViewById(R.id.b5);
                ImageView c5 = (ImageView) v.findViewById(R.id.c5);

                e4.setImageResource(R.mipmap.white_pawn);
                e5.setImageResource(R.mipmap.black_pawn);
                f3.setImageResource(R.mipmap.white_knight);
                c6.setImageResource(R.mipmap.black_knight);
                b5.setImageResource(R.mipmap.white_bishop);
                c5.setImageResource(R.mipmap.black_bishop);

                e4.setVisibility(View.VISIBLE);
                e5.setVisibility(View.VISIBLE);
                f3.setVisibility(View.VISIBLE);
                c6.setVisibility(View.VISIBLE);
                b5.setVisibility(View.VISIBLE);
                c5.setVisibility(View.VISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 20, 27, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 6;
            }
        });

        return v;
    }
}
