package com.samanic.matej.learnchessopenings.FlankOpenings.Zukertort;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ZukertortNimzoLarsenFragment extends Fragment {


    public ZukertortNimzoLarsenFragment() {
        // Required empty public constructor
    }

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.zukertort_nimzo_larsen, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wGKnight = v.findViewById(R.id.whiteGKnight);
        final View bDPawn = v.findViewById(R.id.blackDPawn);
        final View wBPawn = v.findViewById(R.id.whiteBPawn);

        final TextView tw = (TextView) v.findViewById(R.id.zukertortnimzolarsen);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wGKnight.setVisibility(View.VISIBLE);
                bDPawn.setVisibility(View.VISIBLE);
                wBPawn.setVisibility(View.VISIBLE);

                ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                ImageView b3 = (ImageView) v.findViewById(R.id.b3);

                f3.setVisibility(View.INVISIBLE);
                d5.setVisibility(View.INVISIBLE);
                b3.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number){

                    case 3:
                        wBPawn.setVisibility(View.VISIBLE);
                        ImageView b3 = (ImageView) v.findViewById(R.id.b3);
                        b3.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 11, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 2:
                        bDPawn.setVisibility(View.VISIBLE);
                        ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                        d5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 6, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wGKnight.setVisibility(View.VISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 5, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (number) {

                    case 0:
                        wGKnight.setVisibility(View.INVISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setImageResource(R.mipmap.white_knight);
                        f3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 5, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bDPawn.setVisibility(View.INVISIBLE);
                        ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                        d5.setImageResource(R.mipmap.black_pawn);
                        d5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 6, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 2:
                        wBPawn.setVisibility(View.INVISIBLE);
                        ImageView b3 = (ImageView) v.findViewById(R.id.b3);
                        b3.setImageResource(R.mipmap.white_pawn);
                        b3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 11, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wGKnight.setVisibility(View.INVISIBLE);
                bDPawn.setVisibility(View.INVISIBLE);
                wBPawn.setVisibility(View.INVISIBLE);

                ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                ImageView b3 = (ImageView) v.findViewById(R.id.b3);

                f3.setImageResource(R.mipmap.white_knight);
                d5.setImageResource(R.mipmap.black_pawn);
                b3.setImageResource(R.mipmap.white_pawn);

                f3.setVisibility(View.VISIBLE);
                d5.setVisibility(View.VISIBLE);
                b3.setVisibility(View.VISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 11, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 3;
            }
        });

        return v;
    }
}