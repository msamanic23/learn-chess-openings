package com.samanic.matej.learnchessopenings.ClosedGames.SlavDefence;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SlavExchangeFragment extends Fragment {


    public SlavExchangeFragment() {
        // Required empty public constructor
    }

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.slav_exchange, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wDPawn = v.findViewById(R.id.whiteDPawn);
        final View bDPawn = v.findViewById(R.id.blackDPawn);
        final View wCPawn = v.findViewById(R.id.whiteCPawn);
        final View bCPawn = v.findViewById(R.id.blackCPawn);

        final TextView tw = (TextView) v.findViewById(R.id.slavexchange);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wDPawn.setVisibility(View.VISIBLE);
                bDPawn.setVisibility(View.VISIBLE);
                wCPawn.setVisibility(View.VISIBLE);
                bCPawn.setVisibility(View.VISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                ImageView c6 = (ImageView) v.findViewById(R.id.c6);

                d4.setVisibility(View.INVISIBLE);
                d5.setVisibility(View.INVISIBLE);
                c4.setVisibility(View.INVISIBLE);
                c6.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number){


                    case 6:
                        ImageView c6 = (ImageView) v.findViewById(R.id.c6);
                        c6.setImageResource(R.mipmap.black_pawn);
                        c6.setVisibility(View.VISIBLE);

                        ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                        d5.setImageResource(R.mipmap.white_pawn);
                        d5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 23, 27, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 5:
                        ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                        c4.setImageResource(R.mipmap.white_pawn);
                        c4.setVisibility(View.VISIBLE);

                        ImageView dd5 = (ImageView) v.findViewById(R.id.d5);
                        dd5.setImageResource(R.mipmap.black_pawn);
                        dd5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 18, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 4:
                        bCPawn.setVisibility(View.VISIBLE);
                        ImageView cc6 = (ImageView) v.findViewById(R.id.c6);
                        cc6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 13, 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 3:
                        wCPawn.setVisibility(View.VISIBLE);
                        ImageView cc4 = (ImageView) v.findViewById(R.id.c4);
                        cc4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 10, 12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 2:
                        bDPawn.setVisibility(View.VISIBLE);
                        ImageView ddd5 = (ImageView) v.findViewById(R.id.d5);
                        ddd5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wDPawn.setVisibility(View.VISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (number) {
                    case 0:
                        wDPawn.setVisibility(View.INVISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setImageResource(R.mipmap.white_pawn);
                        d4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bDPawn.setVisibility(View.INVISIBLE);
                        ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                        d5.setImageResource(R.mipmap.black_pawn);
                        d5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 2:
                        wCPawn.setVisibility(View.INVISIBLE);
                        ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                        c4.setImageResource(R.mipmap.white_pawn);
                        c4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 3:
                        bCPawn.setVisibility(View.INVISIBLE);
                        ImageView c6 = (ImageView) v.findViewById(R.id.c6);
                        c6.setImageResource(R.mipmap.black_pawn);
                        c6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 13, 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 4:
                        ImageView cc4 = (ImageView) v.findViewById(R.id.c4);
                        cc4.setVisibility(View.INVISIBLE);
                        ImageView dd5 = (ImageView) v.findViewById(R.id.d5);
                        dd5.setImageResource(R.mipmap.white_pawn);
                        dd5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 18, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 5:
                        ImageView cc6 = (ImageView) v.findViewById(R.id.c6);
                        cc6.setVisibility(View.INVISIBLE);
                        ImageView ddd5 = (ImageView) v.findViewById(R.id.d5);
                        ddd5.setImageResource(R.mipmap.black_pawn);
                        ddd5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 23, 27, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wDPawn.setVisibility(View.INVISIBLE);
                bDPawn.setVisibility(View.INVISIBLE);
                wCPawn.setVisibility(View.INVISIBLE);
                bCPawn.setVisibility(View.INVISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                ImageView c6 = (ImageView) v.findViewById(R.id.c6);

                d4.setImageResource(R.mipmap.white_pawn);
                d5.setImageResource(R.mipmap.black_pawn);

                d4.setVisibility(View.VISIBLE);
                d5.setVisibility(View.VISIBLE);
                c4.setVisibility(View.INVISIBLE);
                c6.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 18, 27, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 6;
            }
        });

        return v;
    }
}