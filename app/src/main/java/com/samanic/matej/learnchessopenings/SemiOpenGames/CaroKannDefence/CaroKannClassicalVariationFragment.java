package com.samanic.matej.learnchessopenings.SemiOpenGames.CaroKannDefence;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CaroKannClassicalVariationFragment extends Fragment {


    public CaroKannClassicalVariationFragment() {
        // Required empty public constructor
    }

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.caro_kann_classical_variation, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wEPawn = v.findViewById(R.id.whiteEPawn);
        final View wBKnight = v.findViewById(R.id.whiteBKnight);
        final View bcBishop = v.findViewById(R.id.blackLightBishop);
        final View wDPawn = v.findViewById(R.id.whiteDPawn);
        final View bDPawn = v.findViewById(R.id.blackDPawn);
        final View bCPawn = v.findViewById(R.id.blackCPawn);

        final TextView tw = (TextView) v.findViewById(R.id.carokannclassical);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wEPawn.setVisibility(View.VISIBLE);
                wDPawn.setVisibility(View.VISIBLE);
                bDPawn.setVisibility(View.VISIBLE);
                bCPawn.setVisibility(View.VISIBLE);
                wBKnight.setVisibility(View.VISIBLE);
                bcBishop.setVisibility(View.VISIBLE);

                ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                ImageView c6 = (ImageView) v.findViewById(R.id.c6);
                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                ImageView f5 = (ImageView) v.findViewById(R.id.f5);

                e4.setVisibility(View.INVISIBLE);
                c6.setVisibility(View.INVISIBLE);
                d4.setVisibility(View.INVISIBLE);
                d5.setVisibility(View.INVISIBLE);
                c3.setVisibility(View.INVISIBLE);
                f5.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number) {

                    case 8:
                        bcBishop.setVisibility(View.VISIBLE);
                        ImageView f5 = (ImageView) v.findViewById(R.id.f5);
                        f5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 34, 37, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 7:
                        ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                        e4.setImageResource(R.mipmap.black_pawn);
                        e4.setVisibility(View.VISIBLE);

                        ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                        c3.setImageResource(R.mipmap.white_knight);
                        c3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 29, 33, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 6:
                        ImageView ee4 = (ImageView) v.findViewById(R.id.e4);
                        ee4.setImageResource(R.mipmap.white_pawn);
                        ee4.setVisibility(View.VISIBLE);

                        ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                        d5.setImageResource(R.mipmap.black_pawn);
                        d5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 22, 26, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 5:
                        wBKnight.setVisibility(View.VISIBLE);
                        ImageView cc3 = (ImageView) v.findViewById(R.id.c3);
                        cc3.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 18, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 4:
                        bDPawn.setVisibility(View.VISIBLE);
                        ImageView dd5 = (ImageView) v.findViewById(R.id.d5);
                        dd5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 13, 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 3:
                        wDPawn.setVisibility(View.VISIBLE);
                        ImageView dd4 = (ImageView) v.findViewById(R.id.d4);
                        dd4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 10, 12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 2:
                        bCPawn.setVisibility(View.VISIBLE);
                        ImageView cc6 = (ImageView) v.findViewById(R.id.c6);
                        cc6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wEPawn.setVisibility(View.VISIBLE);
                        ImageView eee4 = (ImageView) v.findViewById(R.id.e4);
                        eee4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (number){
                    case 0:
                        wEPawn.setVisibility(View.INVISIBLE);
                        ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                        e4.setImageResource(R.mipmap.white_pawn);
                        e4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bCPawn.setVisibility(View.INVISIBLE);
                        ImageView c6 = (ImageView) v.findViewById(R.id.c6);
                        c6.setImageResource(R.mipmap.black_pawn);
                        c6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 2:
                        wDPawn.setVisibility(View.INVISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setImageResource(R.mipmap.white_pawn);
                        d4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 3:
                        bDPawn.setVisibility(View.INVISIBLE);
                        ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                        d5.setImageResource(R.mipmap.black_pawn);
                        d5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 13, 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 4:
                        wBKnight.setVisibility(View.INVISIBLE);
                        ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                        c3.setImageResource(R.mipmap.white_knight);
                        c3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 18, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 5:
                        ImageView dd5 = (ImageView) v.findViewById(R.id.d5);
                        dd5.setVisibility(View.INVISIBLE);
                        ImageView ee4 = (ImageView) v.findViewById(R.id.e4);
                        ee4.setImageResource(R.mipmap.black_pawn);
                        ee4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 22, 26, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 6:
                        ImageView cc3 = (ImageView) v.findViewById(R.id.c3);
                        cc3.setVisibility(View.INVISIBLE);
                        ImageView eee4 = (ImageView) v.findViewById(R.id.e4);
                        eee4.setImageResource(R.mipmap.white_knight);
                        eee4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 29, 33, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 7:
                        bcBishop.setVisibility(View.INVISIBLE);
                        ImageView f5 = (ImageView) v.findViewById(R.id.f5);
                        f5.setImageResource(R.mipmap.black_bishop);
                        f5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 34, 37, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }

            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wEPawn.setVisibility(View.INVISIBLE);
                bCPawn.setVisibility(View.INVISIBLE);
                wDPawn.setVisibility(View.INVISIBLE);
                bDPawn.setVisibility(View.INVISIBLE);
                wBKnight.setVisibility(View.INVISIBLE);
                bcBishop.setVisibility(View.INVISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView c6 = (ImageView) v.findViewById(R.id.c6);
                ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                ImageView f5 = (ImageView) v.findViewById(R.id.f5);
                ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);

                d4.setImageResource(R.mipmap.white_pawn);
                c6.setImageResource(R.mipmap.black_pawn);
                e4.setImageResource(R.mipmap.white_knight);
                f5.setImageResource(R.mipmap.black_bishop);

                d4.setVisibility(View.VISIBLE);
                c6.setVisibility(View.VISIBLE);
                e4.setVisibility(View.VISIBLE);
                f5.setVisibility(View.VISIBLE);
                c3.setVisibility(View.INVISIBLE);
                d5.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 18, 26, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 29, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 8;
            }
        });

        return v;
    }
}
