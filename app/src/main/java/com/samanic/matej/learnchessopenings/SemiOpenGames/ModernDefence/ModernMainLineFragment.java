package com.samanic.matej.learnchessopenings.SemiOpenGames.ModernDefence;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ModernMainLineFragment extends Fragment {


    public ModernMainLineFragment() {
        // Required empty public constructor
    }

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.modern_main_line, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wEPawn = v.findViewById(R.id.whiteEPawn);
        final View wDPawn = v.findViewById(R.id.whiteDPawn);
        final View bDBishop = v.findViewById(R.id.blackDarkBishop);
        final View bGPawn = v.findViewById(R.id.blackGPawn);

        final TextView tw = (TextView) v.findViewById(R.id.modernmain);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wEPawn.setVisibility(View.VISIBLE);
                wDPawn.setVisibility(View.VISIBLE);
                bDBishop.setVisibility(View.VISIBLE);
                bGPawn.setVisibility(View.VISIBLE);

                ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView g7 = (ImageView) v.findViewById(R.id.g7);

                e4.setVisibility(View.INVISIBLE);
                g6.setVisibility(View.INVISIBLE);
                d4.setVisibility(View.INVISIBLE);
                g7.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number) {

                    case 4:
                        bDBishop.setVisibility(View.VISIBLE);
                        ImageView g7 = (ImageView) v.findViewById(R.id.g7);
                        g7.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 13, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 3:
                        wDPawn.setVisibility(View.VISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 10, 12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 2:
                        bGPawn.setVisibility(View.VISIBLE);
                        ImageView cg6 = (ImageView) v.findViewById(R.id.g6);
                        cg6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wEPawn.setVisibility(View.VISIBLE);
                        ImageView eee4 = (ImageView) v.findViewById(R.id.e4);
                        eee4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (number) {
                    case 0:
                        wEPawn.setVisibility(View.INVISIBLE);
                        ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                        e4.setImageResource(R.mipmap.white_pawn);
                        e4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bGPawn.setVisibility(View.INVISIBLE);
                        ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                        g6.setImageResource(R.mipmap.black_pawn);
                        g6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 2:
                        wDPawn.setVisibility(View.INVISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setImageResource(R.mipmap.white_pawn);
                        d4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 3:
                        bDBishop.setVisibility(View.INVISIBLE);
                        ImageView g7 = (ImageView) v.findViewById(R.id.g7);
                        g7.setImageResource(R.mipmap.black_bishop);
                        g7.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 13, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }

            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wEPawn.setVisibility(View.INVISIBLE);
                bGPawn.setVisibility(View.INVISIBLE);
                wDPawn.setVisibility(View.INVISIBLE);
                bDBishop.setVisibility(View.INVISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                ImageView e4 = (ImageView) v.findViewById(R.id.e4);
                ImageView g7 = (ImageView) v.findViewById(R.id.g7);

                e4.setImageResource(R.mipmap.white_pawn);
                d4.setImageResource(R.mipmap.white_pawn);
                g7.setImageResource(R.mipmap.black_bishop);
                g6.setImageResource(R.mipmap.black_pawn);

                d4.setVisibility(View.VISIBLE);
                g7.setVisibility(View.VISIBLE);
                e4.setVisibility(View.VISIBLE);
                g6.setVisibility(View.VISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 10, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 4;
            }
        });

        return v;
    }
}