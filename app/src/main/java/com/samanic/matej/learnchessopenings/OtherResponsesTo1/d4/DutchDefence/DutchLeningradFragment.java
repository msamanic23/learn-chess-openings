package com.samanic.matej.learnchessopenings.OtherResponsesTo1.d4.DutchDefence;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;

public class DutchLeningradFragment extends Fragment {


    public DutchLeningradFragment() {
        // Required empty public constructor
    }

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.dutch_leningrad, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wDPawn = v.findViewById(R.id.whiteDPawn);
        final View bFPawn = v.findViewById(R.id.blackFPawn);
        final View wCPawn = v.findViewById(R.id.whiteCPawn);
        final View bGKnight = v.findViewById(R.id.blackGKnight);
        final View wGPawn = v.findViewById(R.id.whiteGPawn);
        final View bGPawn = v.findViewById(R.id.blackGPawn);
        final View wFBishop = v.findViewById(R.id.whiteLightBishop);
        final View bFBishop = v.findViewById(R.id.blackDarkBishop);
        final View wGKnight = v.findViewById(R.id.whiteGKnight);

        final TextView tw = (TextView) v.findViewById(R.id.dutchleningrad);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wDPawn.setVisibility(View.VISIBLE);
                bFPawn.setVisibility(View.VISIBLE);
                wCPawn.setVisibility(View.VISIBLE);
                bGKnight.setVisibility(View.VISIBLE);
                wGPawn.setVisibility(View.VISIBLE);
                bGPawn.setVisibility(View.VISIBLE);
                wFBishop.setVisibility(View.VISIBLE);
                bFBishop.setVisibility(View.VISIBLE);
                wGKnight.setVisibility(View.VISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView f5 = (ImageView) v.findViewById(R.id.f5);
                ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                ImageView g3 = (ImageView) v.findViewById(R.id.g3);
                ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                ImageView g2 = (ImageView) v.findViewById(R.id.g2);
                ImageView g7 = (ImageView) v.findViewById(R.id.g7);
                ImageView f3 = (ImageView) v.findViewById(R.id.f3);

                d4.setVisibility(View.INVISIBLE);
                f5.setVisibility(View.INVISIBLE);
                c4.setVisibility(View.INVISIBLE);
                f6.setVisibility(View.INVISIBLE);
                g3.setVisibility(View.INVISIBLE);
                g6.setVisibility(View.INVISIBLE);
                g2.setVisibility(View.INVISIBLE);
                g7.setVisibility(View.INVISIBLE);
                f3.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number){

                    case 9:
                        wGKnight.setVisibility(View.VISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 37, 40, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 8:
                        bFBishop.setVisibility(View.VISIBLE);
                        ImageView g7 = (ImageView) v.findViewById(R.id.g7);
                        g7.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 31, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 7:
                        wFBishop.setVisibility(View.VISIBLE);
                        ImageView g2 = (ImageView) v.findViewById(R.id.g2);
                        g2.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 27, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 6:
                        bGPawn.setVisibility(View.VISIBLE);
                        ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                        g6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 22, 24, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 5:
                        wGPawn.setVisibility(View.VISIBLE);
                        ImageView g3 = (ImageView) v.findViewById(R.id.g3);
                        g3.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 19, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 4:
                        bGKnight.setVisibility(View.VISIBLE);
                        ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                        f6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 13, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 3:
                        wCPawn.setVisibility(View.VISIBLE);
                        ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                        c4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 10, 12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 2:
                        bFPawn.setVisibility(View.VISIBLE);
                        ImageView f5 = (ImageView) v.findViewById(R.id.f5);
                        f5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wDPawn.setVisibility(View.VISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (number) {
                    case 0:
                        wDPawn.setVisibility(View.INVISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setImageResource(R.mipmap.white_pawn);
                        d4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bFPawn.setVisibility(View.INVISIBLE);
                        ImageView f5 = (ImageView) v.findViewById(R.id.f5);
                        f5.setImageResource(R.mipmap.black_pawn);
                        f5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 2:
                        wCPawn.setVisibility(View.INVISIBLE);
                        ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                        c4.setImageResource(R.mipmap.white_pawn);
                        c4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 3:
                        bGKnight.setVisibility(View.INVISIBLE);
                        ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                        f6.setImageResource(R.mipmap.black_knight);
                        f6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 13, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 4:
                        wGPawn.setVisibility(View.INVISIBLE);
                        ImageView g3 = (ImageView) v.findViewById(R.id.g3);
                        g3.setImageResource(R.mipmap.white_pawn);
                        g3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 19, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 5:
                        bGPawn.setVisibility(View.INVISIBLE);
                        ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                        g6.setImageResource(R.mipmap.black_pawn);
                        g6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 22, 24, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 6:
                        wFBishop.setVisibility(View.INVISIBLE);
                        ImageView g2 = (ImageView) v.findViewById(R.id.g2);
                        g2.setImageResource(R.mipmap.white_bishop);
                        g2.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 27, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 7:
                        bFBishop.setVisibility(View.INVISIBLE);
                        ImageView g7 = (ImageView) v.findViewById(R.id.g7);
                        g7.setImageResource(R.mipmap.black_bishop);
                        g7.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 31, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 8:
                        wGKnight.setVisibility(View.INVISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setImageResource(R.mipmap.white_knight);
                        f3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 37, 40, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wDPawn.setVisibility(View.INVISIBLE);
                bFPawn.setVisibility(View.INVISIBLE);
                wCPawn.setVisibility(View.INVISIBLE);
                bGKnight.setVisibility(View.INVISIBLE);
                wGPawn.setVisibility(View.INVISIBLE);
                bGPawn.setVisibility(View.INVISIBLE);
                wFBishop.setVisibility(View.INVISIBLE);
                bFBishop.setVisibility(View.INVISIBLE);
                wGKnight.setVisibility(View.INVISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView f5 = (ImageView) v.findViewById(R.id.f5);
                ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                ImageView g3 = (ImageView) v.findViewById(R.id.g3);
                ImageView g6 = (ImageView) v.findViewById(R.id.g6);
                ImageView g2 = (ImageView) v.findViewById(R.id.g2);
                ImageView g7 = (ImageView) v.findViewById(R.id.g7);
                ImageView f3 = (ImageView) v.findViewById(R.id.f3);

                d4.setImageResource(R.mipmap.white_pawn);
                f5.setImageResource(R.mipmap.black_pawn);
                c4.setImageResource(R.mipmap.white_pawn);
                f6.setImageResource(R.mipmap.black_knight);
                g3.setImageResource(R.mipmap.white_pawn);
                g6.setImageResource(R.mipmap.black_pawn);
                g2.setImageResource(R.mipmap.white_bishop);
                g7.setImageResource(R.mipmap.black_bishop);
                f3.setImageResource(R.mipmap.white_knight);

                d4.setVisibility(View.VISIBLE);
                f5.setVisibility(View.VISIBLE);
                c4.setVisibility(View.VISIBLE);
                f6.setVisibility(View.VISIBLE);
                g3.setVisibility(View.VISIBLE);
                g6.setVisibility(View.VISIBLE);
                g2.setVisibility(View.VISIBLE);
                g7.setVisibility(View.VISIBLE);
                f3.setVisibility(View.VISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 19, 24, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 27, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 37, 40, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 9;
            }
        });

        return v;
    }
}