package com.samanic.matej.learnchessopenings.ClosedGames.QueensGambitDeclined;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class QGDRagozinFragment extends Fragment {


    public QGDRagozinFragment() {
        // Required empty public constructor
    }

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.qgd_ragozin, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wDPawn = v.findViewById(R.id.whiteDPawn);
        final View bDPawn = v.findViewById(R.id.blackDPawn);
        final View wCPawn = v.findViewById(R.id.whiteCPawn);
        final View bEPawn = v.findViewById(R.id.blackEPawn);
        final View wBKnight = v.findViewById(R.id.whiteBKnight);
        final View bGKnight = v.findViewById(R.id.blackGKnight);
        final View wGKnight = v.findViewById(R.id.whiteGKnight);
        final View bFBishop = v.findViewById(R.id.blackDarkBishop);

        final TextView tw = (TextView) v.findViewById(R.id.qgdragozin);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;


        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wDPawn.setVisibility(View.VISIBLE);
                bDPawn.setVisibility(View.VISIBLE);
                wCPawn.setVisibility(View.VISIBLE);
                bEPawn.setVisibility(View.VISIBLE);
                wBKnight.setVisibility(View.VISIBLE);
                bGKnight.setVisibility(View.VISIBLE);
                wGKnight.setVisibility(View.VISIBLE);
                bFBishop.setVisibility(View.VISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                ImageView e6 = (ImageView) v.findViewById(R.id.e6);
                ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                ImageView b4 = (ImageView) v.findViewById(R.id.b4);

                d4.setVisibility(View.INVISIBLE);
                d5.setVisibility(View.INVISIBLE);
                c4.setVisibility(View.INVISIBLE);
                e6.setVisibility(View.INVISIBLE);
                c3.setVisibility(View.INVISIBLE);
                f6.setVisibility(View.INVISIBLE);
                f3.setVisibility(View.INVISIBLE);
                b4.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number){

                    case 8:
                        bFBishop.setVisibility(View.VISIBLE);
                        ImageView b4 = (ImageView) v.findViewById(R.id.b4);
                        b4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 32, 35, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 7:
                        wGKnight.setVisibility(View.VISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 28, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 6:
                        bGKnight.setVisibility(View.VISIBLE);
                        ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                        f6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 22, 25, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 5:
                        wBKnight.setVisibility(View.VISIBLE);
                        ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                        c3.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 18, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 4:
                        bEPawn.setVisibility(View.VISIBLE);
                        ImageView e6 = (ImageView) v.findViewById(R.id.e6);
                        e6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 13, 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 3:
                        wCPawn.setVisibility(View.VISIBLE);
                        ImageView cc4 = (ImageView) v.findViewById(R.id.c4);
                        cc4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 10, 12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 2:
                        bDPawn.setVisibility(View.VISIBLE);
                        ImageView dd5 = (ImageView) v.findViewById(R.id.d5);
                        dd5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wDPawn.setVisibility(View.VISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (number) {
                    case 0:
                        wDPawn.setVisibility(View.INVISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setImageResource(R.mipmap.white_pawn);
                        d4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bDPawn.setVisibility(View.INVISIBLE);
                        ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                        d5.setImageResource(R.mipmap.black_pawn);
                        d5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 2:
                        wCPawn.setVisibility(View.INVISIBLE);
                        ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                        c4.setImageResource(R.mipmap.white_pawn);
                        c4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 3:
                        bEPawn.setVisibility(View.INVISIBLE);
                        ImageView e6 = (ImageView) v.findViewById(R.id.e6);
                        e6.setImageResource(R.mipmap.black_pawn);
                        e6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 13, 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 4:
                        wBKnight.setVisibility(View.INVISIBLE);
                        ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                        c3.setImageResource(R.mipmap.white_knight);
                        c3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 18, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 5:
                        bGKnight.setVisibility(View.INVISIBLE);
                        ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                        f6.setImageResource(R.mipmap.black_knight);
                        f6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 22, 25, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 6:
                        wGKnight.setVisibility(View.INVISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setImageResource(R.mipmap.white_knight);
                        f3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 28, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 7:
                        bFBishop.setVisibility(View.INVISIBLE);
                        ImageView b4 = (ImageView) v.findViewById(R.id.b4);
                        b4.setImageResource(R.mipmap.black_bishop);
                        b4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 32, 35, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wDPawn.setVisibility(View.INVISIBLE);
                bDPawn.setVisibility(View.INVISIBLE);
                wCPawn.setVisibility(View.INVISIBLE);
                bEPawn.setVisibility(View.INVISIBLE);
                wBKnight.setVisibility(View.INVISIBLE);
                bGKnight.setVisibility(View.INVISIBLE);
                wGKnight.setVisibility(View.INVISIBLE);
                bFBishop.setVisibility(View.INVISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                ImageView c4 = (ImageView) v.findViewById(R.id.c4);
                ImageView e6 = (ImageView) v.findViewById(R.id.e6);
                ImageView c3 = (ImageView) v.findViewById(R.id.c3);
                ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                ImageView b4 = (ImageView) v.findViewById(R.id.b4);

                d4.setImageResource(R.mipmap.white_pawn);
                d5.setImageResource(R.mipmap.black_pawn);
                c4.setImageResource(R.mipmap.white_pawn);
                e6.setImageResource(R.mipmap.black_pawn);
                c3.setImageResource(R.mipmap.white_knight);
                f6.setImageResource(R.mipmap.black_knight);
                f3.setImageResource(R.mipmap.white_knight);
                b4.setImageResource(R.mipmap.black_bishop);

                d4.setVisibility(View.VISIBLE);
                d5.setVisibility(View.VISIBLE);
                c4.setVisibility(View.VISIBLE);
                e6.setVisibility(View.VISIBLE);
                c3.setVisibility(View.VISIBLE);
                f6.setVisibility(View.VISIBLE);
                f3.setVisibility(View.VISIBLE);
                b4.setVisibility(View.VISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 18, 25, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 28, 35, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 8;
            }
        });

        return v;
    }
}