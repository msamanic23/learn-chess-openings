package com.samanic.matej.learnchessopenings;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;


import com.samanic.matej.learnchessopenings.ClosedGames.LondonFragment;
import com.samanic.matej.learnchessopenings.ClosedGames.QueensGambitAccepted.QGATabFragment;
import com.samanic.matej.learnchessopenings.ClosedGames.QueensGambitDeclined.QGDTabFragment;
import com.samanic.matej.learnchessopenings.ClosedGames.SlavDefence.SlavTabFragment;
import com.samanic.matej.learnchessopenings.ClosedGames.TorreFragment;
import com.samanic.matej.learnchessopenings.FlankOpenings.English.EnglishTabFragment;
import com.samanic.matej.learnchessopenings.FlankOpenings.Zukertort.ZukertortTabFragment;
import com.samanic.matej.learnchessopenings.IndianDefence.BenoniDefence.BenoniTabFragment;
import com.samanic.matej.learnchessopenings.IndianDefence.CatalanOpening.CatalanTabFragment;
import com.samanic.matej.learnchessopenings.IndianDefence.GrunfeldDefence.GrunfeldTabFragment;
import com.samanic.matej.learnchessopenings.IndianDefence.KingsIndianDefence.KingsIndianTabFragment;
import com.samanic.matej.learnchessopenings.IndianDefence.NimzoIndianDefence.NimzoIndianTabFragment;
import com.samanic.matej.learnchessopenings.IndianDefence.QueensIndianDefence.QueensIndianTabFragment;
import com.samanic.matej.learnchessopenings.OpenGames.BishopsOpening.BishopTabFragment;
import com.samanic.matej.learnchessopenings.OpenGames.FourKnights.FourKnightsTabFragment;
import com.samanic.matej.learnchessopenings.OpenGames.Italian.ItalianTabFragment;
import com.samanic.matej.learnchessopenings.OpenGames.KingsGambit.KingsGambitTabFragment;
import com.samanic.matej.learnchessopenings.OpenGames.PetrovDefence.PetrovTabFragment;
import com.samanic.matej.learnchessopenings.OpenGames.PhilidorDefence.PhilidorTabFragment;
import com.samanic.matej.learnchessopenings.OpenGames.RuyLopez.RuyTabFragment;
import com.samanic.matej.learnchessopenings.OpenGames.Scotch.ScotchTabFragment;
import com.samanic.matej.learnchessopenings.OtherResponsesTo1.d4.DutchDefence.DutchTabFragment;
import com.samanic.matej.learnchessopenings.OtherResponsesTo1.d4.OldBenoniFragment;
import com.samanic.matej.learnchessopenings.SemiOpenGames.AlekhineDefence.AlekhineTabFragment;
import com.samanic.matej.learnchessopenings.SemiOpenGames.CaroKannDefence.CaroKannTabFragment;
import com.samanic.matej.learnchessopenings.SemiOpenGames.FrenchDefence.FrenchTabFragment;
import com.samanic.matej.learnchessopenings.SemiOpenGames.ModernDefence.ModernTabFragment;
import com.samanic.matej.learnchessopenings.SemiOpenGames.NimzowitchDefence.NimzowitchTabFragment;
import com.samanic.matej.learnchessopenings.SemiOpenGames.PircDefence.PircTabFragment;
import com.samanic.matej.learnchessopenings.SemiOpenGames.ScandinavianDefence.ScandinavianTabFragment;
import com.samanic.matej.learnchessopenings.SemiOpenGames.SicilianDefence.SicilianTabFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    View view_Group;
    DrawerLayout mDrawerLayout;

    ExpandableListAdapter mMenuAdapter;
    ExpandableListView expandableList;
    List<ExpandedMenuModel> listDataHeader;
    HashMap<ExpandedMenuModel, List<String>> listDataChild;

    @Override
    public void onWindowFocusChanged(boolean hasFocus){
        super.onWindowFocusChanged(hasFocus);
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2)
            expandableList.setIndicatorBounds(expandableList.getRight() - 80, expandableList.getWidth());
        else
            expandableList.setIndicatorBoundsRelative(expandableList.getRight()- 80, expandableList.getWidth());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentTransaction fx = getSupportFragmentManager().beginTransaction();
        fx.replace(R.id.containerView, new KarpovFragment()).commit();

        supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        expandableList = (ExpandableListView) findViewById(R.id.navigationmenu);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        if (navigationView != null){
            setupDrawerContent(navigationView);
        }
        prepareListData();
        mMenuAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);


        expandableList.setAdapter(mMenuAdapter);
        expandableList.setOnChildClickListener(new ExpandableListView.OnChildClickListener(){
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long id) {
                view.setSelected(true);

                final String selected = (String) mMenuAdapter.getChild(groupPosition, childPosition);
                if (view_Group != null)
                    view_Group.setBackgroundColor(Color.parseColor("#595955"));

                mDrawerLayout.closeDrawers();

                switch (selected) {
                    case "Spanish Opening":
                        getSupportActionBar().setTitle("Ruy Lopez");
                        FragmentTransaction ruyFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        ruyFragmentTransaction.replace(R.id.containerView, new RuyTabFragment()).commit();
                        break;

                    case "Italian Game":
                        getSupportActionBar().setTitle("Italian Game");
                        FragmentTransaction italianFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        italianFragmentTransaction.replace(R.id.containerView, new ItalianTabFragment()).commit();
                        break;

                    case "Scotch Game":
                        getSupportActionBar().setTitle("Scotch Game");
                        FragmentTransaction scotchFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        scotchFragmentTransaction.replace(R.id.containerView, new ScotchTabFragment()).commit();
                        break;

                    case "Petrov's Defence":
                        getSupportActionBar().setTitle("Petrov's Defence");
                        FragmentTransaction petrovFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        petrovFragmentTransaction.replace(R.id.containerView, new PetrovTabFragment()).commit();
                        break;

                    case "Philidor Defence":
                        getSupportActionBar().setTitle("Philidor Defence");
                        FragmentTransaction philidorFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        philidorFragmentTransaction.replace(R.id.containerView, new PhilidorTabFragment()).commit();
                        break;

                    case "King's Gambit":
                        getSupportActionBar().setTitle("King's Gambit");
                        KingsGambitTabFragment kingsGambitFragment = new KingsGambitTabFragment();
                        FragmentTransaction kingsGambitFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        kingsGambitFragmentTransaction.replace(R.id.containerView, kingsGambitFragment).commit();
                        break;

                    case "Four Knights Game":
                        getSupportActionBar().setTitle("Four Knights Game");
                        FourKnightsTabFragment fourKnightsFragment = new FourKnightsTabFragment();
                        FragmentTransaction fourKnightsFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fourKnightsFragmentTransaction.replace(R.id.containerView, fourKnightsFragment).commit();
                        break;

                    case "Bishop's Opening":
                        getSupportActionBar().setTitle("Bishop's Opening");
                        BishopTabFragment bishopFragment = new BishopTabFragment();
                        FragmentTransaction bishopFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        bishopFragmentTransaction.replace(R.id.containerView, bishopFragment).commit();
                        break;

                    case "Sicilian Defence":
                        getSupportActionBar().setTitle("Sicilian Defence");
                        SicilianTabFragment sicilianFragment = new SicilianTabFragment();
                        FragmentTransaction sicilianFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        sicilianFragmentTransaction.replace(R.id.containerView, sicilianFragment).commit();
                        break;

                    case "French Defence":
                        getSupportActionBar().setTitle("French Defence");
                        FrenchTabFragment frenchFragment = new FrenchTabFragment();
                        FragmentTransaction frenchFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        frenchFragmentTransaction.replace(R.id.containerView, frenchFragment).commit();
                        break;

                    case "Caro-Kann Defence":
                        getSupportActionBar().setTitle("Caro-Kann Defence");
                        CaroKannTabFragment carokannFragment = new CaroKannTabFragment();
                        FragmentTransaction carokannFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        carokannFragmentTransaction.replace(R.id.containerView, carokannFragment).commit();
                        break;

                    case "Pirc Defence":
                        getSupportActionBar().setTitle("Pirc Defence");
                        PircTabFragment pircFragment = new PircTabFragment();
                        FragmentTransaction pircFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        pircFragmentTransaction.replace(R.id.containerView, pircFragment).commit();
                        break;

                    case "Alekhine's Defence":
                        getSupportActionBar().setTitle("Alekhine's Defence");
                        AlekhineTabFragment alekhineFragment = new AlekhineTabFragment();
                        FragmentTransaction alekhineFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        alekhineFragmentTransaction.replace(R.id.containerView, alekhineFragment).commit();
                        break;

                    case "Scandinavian Defence":
                        getSupportActionBar().setTitle("Scandinavian Defence");
                        ScandinavianTabFragment scandinavianFragment = new ScandinavianTabFragment();
                        FragmentTransaction scandinavianFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        scandinavianFragmentTransaction.replace(R.id.containerView, scandinavianFragment).commit();
                        break;

                    case "Modern Defence":
                        getSupportActionBar().setTitle("Modern Defence");
                        ModernTabFragment modernFragment = new ModernTabFragment();
                        FragmentTransaction modernFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        modernFragmentTransaction.replace(R.id.containerView, modernFragment).commit();
                        break;

                    case "Nimzowitsch Defence":
                        getSupportActionBar().setTitle("Nimzowitch Defence");
                        NimzowitchTabFragment fragment2 = new NimzowitchTabFragment();
                        FragmentTransaction frTr = getSupportFragmentManager().beginTransaction();
                        frTr.replace(R.id.containerView, fragment2).commit();
                        break;

                    case "Queen's Gambit Accepted":
                        getSupportActionBar().setTitle("Queen's Gambit Accepted");
                        QGATabFragment qgaFragment = new QGATabFragment();
                        FragmentTransaction qgaFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        qgaFragmentTransaction.replace(R.id.containerView, qgaFragment).commit();
                        break;

                    case "Queen's Gambit Declined":
                        getSupportActionBar().setTitle("Queen's Gambit Declined");
                        QGDTabFragment qgdFragment = new QGDTabFragment();
                        FragmentTransaction qgdFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        qgdFragmentTransaction.replace(R.id.containerView, qgdFragment).commit();
                        break;

                    case "Slav Defence":
                        getSupportActionBar().setTitle("Slav Defence");
                        SlavTabFragment slavFragment = new SlavTabFragment();
                        FragmentTransaction slavFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        slavFragmentTransaction.replace(R.id.containerView, slavFragment).commit();
                        break;

                    case "London System":
                        getSupportActionBar().setTitle("London System");
                        LondonFragment londonFragment = new LondonFragment();
                        FragmentTransaction londonFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        londonFragmentTransaction.replace(R.id.containerView, londonFragment).commit();
                        break;

                    case "Torre Attack":
                        getSupportActionBar().setTitle("Torre Attack");
                        TorreFragment fragment = new TorreFragment();
                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.containerView, fragment).commit();
                        break;

                    case "King's Indian Defence":
                        getSupportActionBar().setTitle("King's Indian Defence");
                        KingsIndianTabFragment kingsindianFragment = new KingsIndianTabFragment();
                        FragmentTransaction kingsindianFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        kingsindianFragmentTransaction.replace(R.id.containerView, kingsindianFragment).commit();
                        break;

                    case "Queen's Indian Defence":
                        getSupportActionBar().setTitle("Queen's Indian Defence");
                        QueensIndianTabFragment queensindianFragment = new QueensIndianTabFragment();
                        FragmentTransaction queensindianFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        queensindianFragmentTransaction.replace(R.id.containerView, queensindianFragment).commit();
                        break;

                    case "Nimzo-Indian Defence":
                        getSupportActionBar().setTitle("Nimzo Indian Defence");
                        NimzoIndianTabFragment nimzoindianFragment = new NimzoIndianTabFragment();
                        FragmentTransaction nimzoindianFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        nimzoindianFragmentTransaction.replace(R.id.containerView, nimzoindianFragment).commit();
                        break;

                    case "Grunfeld Defence":
                        getSupportActionBar().setTitle("Grunfeld Defence");
                        GrunfeldTabFragment grunfeldFragment = new GrunfeldTabFragment();
                        FragmentTransaction grunfeldFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        grunfeldFragmentTransaction.replace(R.id.containerView, grunfeldFragment).commit();
                        break;

                    case "Benoni Defence":
                        getSupportActionBar().setTitle("Benoni Defence");
                        BenoniTabFragment benoniFragment = new BenoniTabFragment();
                        FragmentTransaction benoniFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        benoniFragmentTransaction.replace(R.id.containerView, benoniFragment).commit();
                        break;

                    case "Catalan Opening":
                        getSupportActionBar().setTitle("Catalan Opening");
                        CatalanTabFragment catalanFragment = new CatalanTabFragment();
                        FragmentTransaction catalanFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        catalanFragmentTransaction.replace(R.id.containerView, catalanFragment).commit();
                        break;

                    case "Dutch Defence":
                        getSupportActionBar().setTitle("Dutch Defence");
                        DutchTabFragment dutchFragment = new DutchTabFragment();
                        FragmentTransaction dutchFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        dutchFragmentTransaction.replace(R.id.containerView, dutchFragment).commit();
                        break;

                    case "Old Benoni Defence":
                        getSupportActionBar().setTitle("Old Benoni Defence");
                        OldBenoniFragment oldBenoniFragment = new OldBenoniFragment();
                        FragmentTransaction oldbenoniFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        oldbenoniFragmentTransaction.replace(R.id.containerView, oldBenoniFragment).commit();
                        break;

                    case "English Opening":
                        getSupportActionBar().setTitle("English Opening");
                        EnglishTabFragment englishFragment = new EnglishTabFragment();
                        FragmentTransaction englishFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        englishFragmentTransaction.replace(R.id.containerView, englishFragment).commit();
                        break;

                    case "Zukertort Opening":
                        getSupportActionBar().setTitle("Zukertort Opening");
                        ZukertortTabFragment zukertortFragment = new ZukertortTabFragment();
                        FragmentTransaction zukertortFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        zukertortFragmentTransaction.replace(R.id.containerView, zukertortFragment).commit();
                        break;

                }
               // FragmentManager fragmentManager = getSupportFragmentManager();
                //fragmentManager.beginTransaction().replace(R.id.contann, fragment).commit();
                return false;
            }
        });
        expandableList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                return false;
            }
        });
    }

    public void prepareListData(){
        listDataHeader = new ArrayList<ExpandedMenuModel>();
        listDataChild = new HashMap<ExpandedMenuModel, List<String>>();

        ExpandedMenuModel heading1 = new ExpandedMenuModel();
        heading1.setIconName("Open games: 1.e4 e5");
        listDataHeader.add(heading1);

        List<String> subitem1 = new ArrayList<String>();
        subitem1.add("Spanish Opening");
        subitem1.add("Italian Game");
        subitem1.add("Scotch Game");
        subitem1.add("Petrov's Defence");
        subitem1.add("Philidor Defence");
        subitem1.add("King's Gambit");
        subitem1.add("Four Knights Game");
        subitem1.add("Bishop's Opening");

        ExpandedMenuModel heading2 = new ExpandedMenuModel();
        heading2.setIconName("Semi-open games: 1.e4 !e5");
        listDataHeader.add(heading2);

        List<String> subitem2 = new ArrayList<String>();
        subitem2.add("Sicilian Defence");
        subitem2.add("French Defence");
        subitem2.add("Caro-Kann Defence");
        subitem2.add("Pirc Defence");
        subitem2.add("Alekhine's Defence");
        subitem2.add("Scandinavian Defence");
        subitem2.add("Modern Defence");
        subitem2.add("Nimzowitsch Defence");

        ExpandedMenuModel heading3 = new ExpandedMenuModel();
        heading3.setIconName("Closed games: 1.d4 d5");
        listDataHeader.add(heading3);

        List<String> subitem3 = new ArrayList<String>();
        subitem3.add("Queen's Gambit Accepted");
        subitem3.add("Queen's Gambit Declined");
        subitem3.add("Slav Defence");
        subitem3.add("London System");
        subitem3.add("Torre Attack");

        ExpandedMenuModel heading4 = new ExpandedMenuModel();
        heading4.setIconName("Indian Defence Systems: 1.d4 Nf6");
        listDataHeader.add(heading4);

        List<String> subitem4 = new ArrayList<String>();
        subitem4.add("King's Indian Defence");
        subitem4.add("Queen's Indian Defence");
        subitem4.add("Nimzo-Indian Defence");
        subitem4.add("Grunfeld Defence");
        subitem4.add("Benoni Defence");
        subitem4.add("Catalan Opening");

        ExpandedMenuModel heading5 = new ExpandedMenuModel();
        heading5.setIconName("Other Black responses to: 1.d4");
        listDataHeader.add(heading5);

        List<String> subitem5 = new ArrayList<String>();
        subitem5.add("Dutch Defence");
        subitem5.add("Old Benoni Defence");

        ExpandedMenuModel heading6 = new ExpandedMenuModel();
        heading6.setIconName("Flank Openings");
        listDataHeader.add(heading6);

        List<String> subitem6 = new ArrayList<String>();
        subitem6.add("English Opening");
        subitem6.add("Zukertort Opening");


        listDataChild.put(listDataHeader.get(0), subitem1);
        listDataChild.put(listDataHeader.get(1), subitem2);
        listDataChild.put(listDataHeader.get(2), subitem3);
        listDataChild.put(listDataHeader.get(3), subitem4);
        listDataChild.put(listDataHeader.get(4), subitem5);
        listDataChild.put(listDataHeader.get(5), subitem6);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();

                        return true;
                    }
                });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(final MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
