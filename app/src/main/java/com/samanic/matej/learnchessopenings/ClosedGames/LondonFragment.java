package com.samanic.matej.learnchessopenings.ClosedGames;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LondonFragment extends Fragment {


    public LondonFragment() {
        // Required empty public constructor
    }

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_london, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wDPawn = v.findViewById(R.id.whiteDPawn);
        final View bGKnight = v.findViewById(R.id.blackGKnight);
        final View wGKnight = v.findViewById(R.id.whiteGKnight);
        final View bEPawn = v.findViewById(R.id.blackEPawn);
        final View wDBishop = v.findViewById(R.id.whiteDarkBishop);

        final TextView tw = (TextView) v.findViewById(R.id.london);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wDPawn.setVisibility(View.VISIBLE);
                bGKnight.setVisibility(View.VISIBLE);
                wGKnight.setVisibility(View.VISIBLE);
                bEPawn.setVisibility(View.VISIBLE);
                wDBishop.setVisibility(View.VISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                ImageView e6 = (ImageView) v.findViewById(R.id.e6);
                ImageView f4 = (ImageView) v.findViewById(R.id.f4);

                d4.setVisibility(View.INVISIBLE);
                f6.setVisibility(View.INVISIBLE);
                f3.setVisibility(View.INVISIBLE);
                e6.setVisibility(View.INVISIBLE);
                f4.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number){

                    case 5:
                        wDBishop.setVisibility(View.VISIBLE);
                        ImageView f4 = (ImageView) v.findViewById(R.id.f4);
                        f4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 20, 23, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 4:
                        bEPawn.setVisibility(View.VISIBLE);
                        ImageView e6 = (ImageView) v.findViewById(R.id.e6);
                        e6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 15, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 3:
                        wGKnight.setVisibility(View.VISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 11, 14, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 2:
                        bGKnight.setVisibility(View.VISIBLE);
                        ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                        f6.setImageResource(R.mipmap.black_knight);
                        f6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 5, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wDPawn.setVisibility(View.VISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (number) {
                    case 0:
                        wDPawn.setVisibility(View.INVISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setImageResource(R.mipmap.white_pawn);
                        d4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bGKnight.setVisibility(View.INVISIBLE);
                        ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                        f6.setImageResource(R.mipmap.black_knight);
                        f6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 5, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 2:
                        wGKnight.setVisibility(View.INVISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setImageResource(R.mipmap.white_knight);
                        f3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 11, 14, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 3:
                        bEPawn.setVisibility(View.INVISIBLE);
                        ImageView e6 = (ImageView) v.findViewById(R.id.e6);
                        e6.setImageResource(R.mipmap.black_pawn);
                        e6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 15, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 4:
                        wDBishop.setVisibility(View.INVISIBLE);
                        ImageView f4 = (ImageView) v.findViewById(R.id.f4);
                        f4.setImageResource(R.mipmap.white_bishop);
                        f4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 20, 23, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wDPawn.setVisibility(View.INVISIBLE);
                bGKnight.setVisibility(View.INVISIBLE);
                wGKnight.setVisibility(View.INVISIBLE);
                bEPawn.setVisibility(View.INVISIBLE);
                wDBishop.setVisibility(View.INVISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                ImageView e6 = (ImageView) v.findViewById(R.id.e6);
                ImageView f4 = (ImageView) v.findViewById(R.id.f4);

                d4.setImageResource(R.mipmap.white_pawn);
                f6.setImageResource(R.mipmap.black_knight);
                f3.setImageResource(R.mipmap.white_knight);
                e6.setImageResource(R.mipmap.black_pawn);
                f4.setImageResource(R.mipmap.white_bishop);

                d4.setVisibility(View.VISIBLE);
                f6.setVisibility(View.VISIBLE);
                f3.setVisibility(View.VISIBLE);
                e6.setVisibility(View.VISIBLE);
                f4.setVisibility(View.VISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 11, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 20, 23, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 5;
            }
        });

        return v;
    }
}