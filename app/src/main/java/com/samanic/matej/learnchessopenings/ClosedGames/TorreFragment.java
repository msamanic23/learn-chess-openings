package com.samanic.matej.learnchessopenings.ClosedGames;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.samanic.matej.learnchessopenings.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class TorreFragment extends Fragment {


    public TorreFragment() {
        // Required empty public constructor
    }

    int number = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_torre, container, false);

        ImageButton start = (ImageButton) v.findViewById(R.id.start);
        ImageButton previous = (ImageButton) v.findViewById(R.id.previous);
        ImageButton next = (ImageButton) v.findViewById(R.id.next);
        ImageButton finish = (ImageButton) v.findViewById(R.id.finish);

        final View wDPawn = v.findViewById(R.id.whiteDPawn);
        final View bDPawn = v.findViewById(R.id.blackDPawn);
        final View wGKnight = v.findViewById(R.id.whiteGKnight);
        final View bGKnight = v.findViewById(R.id.blackGKnight);
        final View wDBishop = v.findViewById(R.id.whiteDarkBishop);

        final TextView tw = (TextView) v.findViewById(R.id.torre);
        String text = tw.getText().toString();
        final Spannable ss = new SpannableString(text);
        final int bcgcolor = Color.BLACK;

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wDPawn.setVisibility(View.VISIBLE);
                bDPawn.setVisibility(View.VISIBLE);
                wGKnight.setVisibility(View.VISIBLE);
                bGKnight.setVisibility(View.VISIBLE);
                wDBishop.setVisibility(View.VISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                ImageView g5 = (ImageView) v.findViewById(R.id.g5);

                d4.setVisibility(View.INVISIBLE);
                d5.setVisibility(View.INVISIBLE);
                f3.setVisibility(View.INVISIBLE);
                f6.setVisibility(View.INVISIBLE);
                g5.setVisibility(View.INVISIBLE);

                ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 0;
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(number){

                    case 5:
                        wDBishop.setVisibility(View.VISIBLE);
                        ImageView g5 = (ImageView) v.findViewById(R.id.g5);
                        g5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 20, 23, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 4:
                        bGKnight.setVisibility(View.VISIBLE);
                        ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                        f6.setImageResource(R.mipmap.black_knight);
                        f6.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 14, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 3:
                        wGKnight.setVisibility(View.VISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 10, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 2:
                        bDPawn.setVisibility(View.VISIBLE);
                        ImageView dd5 = (ImageView) v.findViewById(R.id.d5);
                        dd5.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;

                    case 1:
                        wDPawn.setVisibility(View.VISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setVisibility(View.INVISIBLE);

                        ss.setSpan(new ForegroundColorSpan(bcgcolor), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number--;
                        break;
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (number) {
                    case 0:
                        wDPawn.setVisibility(View.INVISIBLE);
                        ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                        d4.setImageResource(R.mipmap.white_pawn);
                        d4.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 1:
                        bDPawn.setVisibility(View.INVISIBLE);
                        ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                        d5.setImageResource(R.mipmap.black_pawn);
                        d5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 5, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 2:
                        wGKnight.setVisibility(View.INVISIBLE);
                        ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                        f3.setImageResource(R.mipmap.white_knight);
                        f3.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 3:
                        bGKnight.setVisibility(View.INVISIBLE);
                        ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                        f6.setImageResource(R.mipmap.black_knight);
                        f6.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 14, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;

                    case 4:
                        wDBishop.setVisibility(View.INVISIBLE);
                        ImageView g5 = (ImageView) v.findViewById(R.id.g5);
                        g5.setImageResource(R.mipmap.white_bishop);
                        g5.setVisibility(View.VISIBLE);

                        ss.setSpan(new ForegroundColorSpan(Color.RED), 20, 23, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tw.setText(ss);
                        number++;
                        break;
                }
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wDPawn.setVisibility(View.INVISIBLE);
                bDPawn.setVisibility(View.INVISIBLE);
                wGKnight.setVisibility(View.INVISIBLE);
                bGKnight.setVisibility(View.INVISIBLE);
                wDBishop.setVisibility(View.INVISIBLE);

                ImageView d4 = (ImageView) v.findViewById(R.id.d4);
                ImageView d5 = (ImageView) v.findViewById(R.id.d5);
                ImageView f3 = (ImageView) v.findViewById(R.id.f3);
                ImageView f6 = (ImageView) v.findViewById(R.id.f6);
                ImageView g5 = (ImageView) v.findViewById(R.id.g5);

                d4.setImageResource(R.mipmap.white_pawn);
                d5.setImageResource(R.mipmap.black_pawn);
                f3.setImageResource(R.mipmap.white_knight);
                f6.setImageResource(R.mipmap.black_knight);
                g5.setImageResource(R.mipmap.white_bishop);

                d4.setVisibility(View.VISIBLE);
                d5.setVisibility(View.VISIBLE);
                f3.setVisibility(View.VISIBLE);
                f6.setVisibility(View.VISIBLE);
                g5.setVisibility(View.VISIBLE);

                ss.setSpan(new ForegroundColorSpan(Color.RED), 2, 7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 10, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.RED), 20, 23, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tw.setText(ss);
                number = 5;
            }
        });

        return v;
    }
}