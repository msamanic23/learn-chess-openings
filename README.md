### README ###

"Learn chess openings" is an Android application which shows every relevant chess opening that is played on the highest level.
You need Android smartphone with minimum version 4.4. to run the application.
To try the application on your phone, you need to install app-release.apk file from 'Learn Chess Openings / app /'